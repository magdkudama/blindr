from flask import Flask, session, request, redirect, url_for, Response
from lib.clients import OAuthHandler, Geo
from pymongo import MongoClient, GEO2D
from bson.json_util import dumps

app = Flask(__name__)

app.config.from_object('lib.config')
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

twitter_api = OAuthHandler(
	app.config['TWITTER_CONSUMER_KEY'],
	app.config['TWITTER_CONSUMER_SECRET'],
	session
)

client = MongoClient(app.config['MONGO_URI'])
db = client[app.config['MONGO_DB_NAME']]

geo = Geo(app.config['GEO_IP_URI'])

@app.route('/')
def main():
	return '<a href="' + twitter_api.get_auth_url() + '">Login</a>'

@app.route('/callback')
def login_callback():
	verifier = request.args.get('oauth_verifier')
	token = request.args.get('oauth_token')

	api = twitter_api.login(token, verifier)

	if api is None:
		return redirect(url_for('main'))

	me = api.me()

	user = {
		"id": me.id,
		"name": me.name,
		"url": me.url,
		"description": me.description,
		"profile_image": me.profile_image_url,
		"location": me.location,
		"lang": me.lang,
		"slug": me.screen_name,
		"location": geo.get_location(request.remote_addr)
	}

	users = db.users
	favorites = db.favorites

	users.create_index([("location", GEO2D)])
	user_id = users.insert_one(user).inserted_id

	for fav in api.favorites():
		favorite = {
			"id": fav.id,
			"text": fav.text,
			"retweeted": fav.retweeted,
			"source": fav.source,
			"source_url": fav.source_url,
			"reply_to": fav.in_reply_to_status_id,
			"user_id": me.id
		}

		favorites.insert_one(favorite)

	return redirect(url_for('main'))

@app.route('/user/<username>')
def get_user(username):
	user = db.users.find_one({"slug": username})

	if user is None:
		return '', 404
	
	return Response(
		dumps(user),
		mimetype='application/json'
	)

@app.route('/user/<username>/matches')
def get_matches(username):
	users = db.users
	favorites = db.favorites
	
	user = users.find_one({"slug": username})

	if user is None:
		return '', 404

	limit = int(request.args.get('limit', 20))

	near = users.find({
		"location": {"$near": user['location']},
		#"$id": { "$not": user['_id'] }
	}).limit(limit)

	result = []
	for doc in near:
		result.append({
			'user': doc,
			'tweets': favorites.find({ 'user_id': doc['id'] })
		})

	return Response(
		dumps(result	),
		mimetype='application/json'
	)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
