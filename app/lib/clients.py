import tweepy
import requests

class Geo:
    
    def __init__(self, geo_url):
        self.__url = geo_url

    def get_location(self, ip):
        result = requests.get(self.__url + ip).json()
        return [result['longitude'], result['latitude']]


class OAuthHandler:

    def __init__(self, consumer_key, consumer_secret, session):
        self.__auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        self.__session = session

    def get_auth_url(self):
        try:
            return self.__auth.get_authorization_url()
        except tweepy.TweepError:
            return ''

    def login(self, token, verifier):
        self.__auth.request_token = {
            'oauth_token': token,
            'oauth_token_secret': verifier
        }

        try:
            get_token = self.__auth.get_access_token(verifier)
            self.__session['tokens'] = get_token
            return tweepy.API(self.__auth)
        except tweepy.TweepError:
            del self.__session['tokens']
            return None
